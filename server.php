<?php

abstract class Handler
{
    const BASE_URL = "http://www.lipsum.com/feed/json";
    public function __construct() {}
    private function __clone() {}
   

    public static function sendRequest($type, $amount, $start) 
    {
        $start = ($start === true) ? "yes" : "no";
        $url = static::BASE_URL.'?'.http_build_query(array("what" => $type, "amount" => $amount, "start" => $start));

        $data = file_get_contents($url);
        $json = json_decode($data, true);

        return $json['feed']['lipsum'];
      }

}

echo Handler::sendRequest("words",100,true);

?>